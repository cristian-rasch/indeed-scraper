require_relative "scraper/version"
require "dotenv"
Dotenv.load
require "nokogiri"
require "open-uri"
require "chronic"
require "set"
require "logger"

module Indeed
  class Scraper
    DEFAULT_LIMIT = 100
    DEFAULT_DATE_RANGE = 15
    DEFAULT_COUNTRY = :us
    BASE_URLS = { us: "http://www.indeed.com", uk: "http://www.indeed.co.uk", au: "http://au.indeed.com" }.freeze
    VALID_DATE_RANGES = [1, 3, 7, 15].freeze
    VALID_JOB_TYPES = %w(contract temporary).freeze
    VALID_JOB_SOURCES = %w(jobsite employer).freeze
    VALID_COUNTRIES = BASE_URLS.keys.freeze
    MAX_PAGE = 25
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze
    SEARCH_PATH = "/jobs".freeze
    REDIRECT_URL_REGEXP = %r(/clk\?)

    class Job < Struct.new(:id, :title, :url, :employer, :location, :short_summary,
                           :created_on, :salary, :description_html)
    end

    # Options include:
    #   - country - which country to search jobs in - defaults to :us, available options also include :uk and :au
    #   - date_rage - how far back to search for results
    #                 (defaults to anytime, valid values are 1, 3, 7 and 15)
    #   - job_types - restrict the search to the specified job types
    #                 (defaults to temporary and contract jobs)
    #   - direct_hire - whether or not to exclude staffing agencies
    #                   (default to true, which means that staffig agencies are excluded by default)
    #   - jobs_source - where to find jobs from. Defaults to all websites.
    #                   Other possible values are jobsite and employer
    #   - salary - minimum annual salary, e.g. 50000
    #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
    #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the INDEED_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - country - which country to search jobs in - defaults to :us, available options also include :uk and :au
    #   - date_rage - how far back to search for results
    #                 (defaults to anytime, valid values are 1, 3, 7 and 15)
    #   - job_types - restrict the search to the specified job types
    #                 (defaults to temporary and contract jobs)
    #   - direct_hire - whether or not to exclude staffing agencies
    #                   (default to true, which means that staffig agencies are excluded by default)
    #   - jobs_source - where to find jobs from. Defaults to all websites.
    #                   Other possible values are jobsite and employer
    #   - salary - minimum annual salary, e.g. 50000
    #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
    #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the INDEED_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])
      configure_proxies(options[:proxies])

      limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
      limit = [limit, DEFAULT_LIMIT].min

      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      max_page = [max_page, MAX_PAGE].min

      country = (options[:country] || @options[:country] || DEFAULT_COUNTRY)
      country = (VALID_COUNTRIES & Array(country)).first || DEFAULT_COUNTRY

      kws = CGI.escape(Array(keywords).join(" "))
      search_url = search_url_for(country)
      search_url << "?as_and=#{kws}&jt=:job_type&limit=50"

      date_range = if options.key?(:date_range)
                     options[:date_range]
                   elsif @options.key?(:date_range)
                     @options[:date_range]
                   end
      date_range = (VALID_DATE_RANGES & Array(date_range)).first
      search_url << "&from_age=#{date_range}" if date_range

      search_url << "&sr=directhire" if options[:direct_hire] || @options[:direct_hire]

      job_source = options[:job_source] || @options[:job_source]
      job_source = (VALID_JOB_SOURCES & Array(job_source)).first
      search_url << "&st=#{job_source}" if job_source

      salary = begin
                 Integer(options[:salary] || @options[:salary])
               rescue TypeError; end
      search_url << "&salary=#{salary}" if salary

      job_types = Array(options[:job_types] || @options[:job_types])
      job_types &= VALID_JOB_TYPES
      job_types = VALID_JOB_TYPES if job_types.empty?

      job_ids = Set.new
      jobs = []
      current_page = 1

      catch(:done) do
        begin
          empty = job_types.map do |job_type|
            jobs_url = search_url.sub(":job_type", job_type)
            jobs_url << "&start=#{50 * (current_page - 1)}"
            io = open_through_proxy(jobs_url)
            next(true) unless io

            jobs_page = Nokogiri::HTML(io)
            job_nodes = jobs_page.css("[data-jk]")

            if job_nodes.empty?
              true
            else
              job_nodes.each do |job_node|
                id = job_node["data-jk"]
                next if job_ids.include?(id)

                title_link = job_node.at_css("a[title]")
                title = title_link["title"]
                job_path = title_link["href"]
                job_url = "#{BASE_URLS[country]}#{job_path}"
                employer_node = job_node.at_css('[itemprop="name"]')
                employer = employer_node.text if employer_node
                location_node = job_node.at_css('[itemprop="addressLocality"]')
                location = location_node.text if location_node
                short_summary_node = job_node.at_css('[itemprop="description"]')
                short_summary = short_summary_node.text.strip if short_summary_node
                created_on = Chronic.parse(job_node.at_css(".date").text.gsub(/[^\w ]/, "")).utc
                salary_node = job_node.at_css("nobr")
                salary = salary_node.text if salary_node

                if job_path !~ REDIRECT_URL_REGEXP && (io = open_through_proxy(job_url))
                  job_page = Nokogiri::HTML(io)
                  description_html = job_page.at_css("#job_summary").inner_html.rstrip
                end

                job = Job.new(id, title, job_url, employer, location, short_summary,
                              created_on, salary, description_html)
                yield(job) if block_given?
                jobs << job
                job_ids << id

                throw(:done) if jobs.size == limit
              end

              false
            end
          end

          current_page += 1 unless done = empty.all? || current_page == max_page
        end until done
      end

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def search_url_for(country)
      "#{BASE_URLS[country]}#{SEARCH_PATH}"
    end

    def configure_proxies(proxies)
      @proxies = proxies || @options[:proxies] || ENV.fetch("INDEED_PROXIES").split(",")
    end

    def open_through_proxy(url)
      begin
        open(url, "User-Agent" => USER_AGENT, "proxy" => next_proxy)
      rescue => err
        @logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    def next_proxy
      @proxies.shift.tap { |proxy| @proxies << proxy }
    end
  end
end
