require File.expand_path("test/test_helper")
require File.expand_path("lib/indeed/scraper")

module Indeed
  class ScraperTest < Minitest::Test
    def setup
      @indeed = Scraper.new
      @keywords = "web developer"
    end

    def test_country_search
      jobs = @indeed.job_search(@keywords, limit: 1, country: :uk)
      assert_equal 1, jobs.size
    end

    def test_date_range_filtering
      jobs = @indeed.job_search(@keywords, limit: 1, date_range: 3)
      assert_equal 1, jobs.size
    end

    def test_date_job_type_filtering
      jobs = @indeed.job_search(@keywords, limit: 1, job_types: "contract")
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @indeed.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_max_page_option
      jobs = @indeed.job_search(@keywords, max_page: 1, limit: 150)
      assert_operator jobs.size, :<=, 150
    end

    def test_disabling_direct_hire
      jobs = @indeed.job_search(@keywords, limit: 1, direct_hire: false)
      assert_equal 1, jobs.size
    end

    def test_restricting_the_job_source
      jobs = @indeed.job_search(@keywords, limit: 1, job_source: "jobsite")
      assert_equal 1, jobs.size
    end

    def test_salary_filtering
      jobs = @indeed.job_search(@keywords, limit: 1, salary: 50_000)
      assert_equal 1, jobs.size
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @indeed.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
