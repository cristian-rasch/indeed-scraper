# Indeed::Scraper

indeed.com job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'indeed-scraper', git: 'git@bitbucket.org:cristian-rasch/indeed-scraper.git', require: 'indeed/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "indeed/scraper"
require "pp"

# Options include:
#   - country - which country to search jobs in - defaults to :us, available options also include :uk and :au
#   - date_rage - how far back to search for results
#                 (defaults to anytime, valid values are 1, 3, 7 and 15)
#   - job_types - restrict the search to the specified job types
#                 (defaults to temporary and contract jobs)
#   - direct_hire - whether or not to exclude staffing agencies
#                   (default to true, which means that staffig agencies are excluded by default)
#   - jobs_source - where to find jobs from. Defaults to all websites.
#                   Other possible values are jobsite and employer
#   - salary - minimum annual salary, e.g. 50000
#   - limit - how many jobs to retrieve (defaults to 20, max. 20)
#   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
#               defaults to reading them from the INDEED_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to
indeed = Indeed::Scraper.new
jobs = indeed.job_search("web developer", date_range: 7, job_types: %w(temporary), job_source: "employer", salary: 30_000, limit: 1)
pp jobs.frirst
# #<struct Indeed::Scraper::Job
#  id="80212dcc5129fbb1",
#  title="Web Developer",
#  url=nil,
#  employer="Virginia Tech",
#  location="Blacksburg, VA",
#  short_summary=
#   "Provides web development, programming, and administrative support for undergraduate admissions. Demonstrated experience in web technologies such as HTML , CSS ,...",
#  created_on=2015-03-04 15:19:46 UTC,
#  salary="$40,000 a year",
#  description_html=nil>
```
